Portfolio
=========

Portfolio of projects
Games folder contains game projects listed below, fiction contains all my works of fiction


- Mastermind - Project for Computer Graphics class. Shared programming duties with another student.
- Artisanne (Prototype) - Project for a class. Contributed to design with a group of 5 students and shared programming duties with another single student. Specifically worked on combat, health systems, enemy AI, and platforming mechanics.
- Dungeon Tales (Prototype) - Senior Project for school. Created solo with use of sprites from the internet.
- Resistor Cal (Resistor Reader) - A mobile app designed to identify the resistances of resistors after taking a picture of them (though that was never achieved during the course). Worked primarily on the back end calculations in order to calculate proper resistances. (Version included is just a basic resistor calculator which contains all of my work)